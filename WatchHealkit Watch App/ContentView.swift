//
//  ContentView.swift
//  WatchHealkit Watch App
//
//  Created by macbook27 on 16/02/24.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var viewModel = WatchConnectViewModel()
    var body: some View {
        VStack(spacing: 20) {
            Text(viewModel.reachable ? "Rechable" : "Not reachable")
            Text(viewModel.currentHeartRat)
                .font(.largeTitle)
//            Text(viewModel.recivedMsg)
//            Button {
//                viewModel.sendDataToiOSApp()
//            } label: {
//                Text("Send data to iPhone")
//            }

        }
        .padding()
        .onAppear {
            viewModel.establishConnection()
            viewModel.requestHealthKitAuthorization()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
