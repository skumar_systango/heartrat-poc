//
//  WatchHealkitApp.swift
//  WatchHealkit Watch App
//
//  Created by macbook27 on 16/02/24.
//

import SwiftUI

@main
struct WatchHealkit_Watch_AppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
