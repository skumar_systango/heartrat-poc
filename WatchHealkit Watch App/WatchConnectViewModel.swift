//
//  WatchConnectViewModel.swift
//  WatchHealkit Watch App
//
//  Created by macbook27 on 16/02/24.
//

import Foundation
import SwiftUI
import WatchConnectivity
import HealthKit

class WatchConnectViewModel: NSObject, ObservableObject, WCSessionDelegate {

    let session = WCSession.default
    @Published var reachable: Bool = false
    @Published var recivedMsg = ""
    @Published var currentHeartRat = "updating now"
    @Published var count = 0
    

    //For health
    //Healthkit
    private let heartRateQuantityType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)
    private let heartRateType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
    private let heartRateUnit = HKUnit(from: "count/min")
    private var workoutSession: HKWorkoutSession?
    private var heartRate = 0
    
    func establishConnection()  {
        if WCSession.isSupported() {
            session.delegate = self
            session.activate()
        }
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        if let error = error {
            print("activation failed with error: \(error.localizedDescription)")
        }
        if WCSession.default.isReachable {
            print("Reachable in watch ")
        } else {
            print("not reachable in watch")
        }
        // Handle activation completion
        print(activationState.rawValue)
    }
    

//    func sendDataToiOSApp() {
//        if session.isReachable {
//            count += 1
//            print("session.isReachable in watch", session.isReachable)
//            let data = ["message": "Hello from watch app \(count)"]
//            session.sendMessage(data, replyHandler: nil, errorHandler: nil)
//        }
//    }

    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        // Handle received message from iOS app
        if let message = message["message"] as? String {
            recivedMsg = message
            print("Received message from iOS app: \(message)")
        }
    }
    
    func sessionReachabilityDidChange(_ session: WCSession) {
        print("sessionReachabilityDidChange in app")
        print(session.isReachable)
        if WCSession.default.activationState == .activated {
            print("activated in app")
            reachable = true
        }
    }
}

//MARK: HealthKit Data
extension WatchConnectViewModel {
  
  func requestHealthKitAuthorization() {
    guard let heartRateQuantityType = heartRateQuantityType else {
      return
    }
    let dataTypes = Set(arrayLiteral: heartRateQuantityType)
    HKHealthStore.sharedInstance?.requestAuthorization(toShare: nil, read: dataTypes) {
      [unowned self] success, error in
      guard success else {
        return
      }
      self.createHealthKitSession()
    }
  }
  
  func createHealthKitSession() {
    // Create a new workout session
    guard let healthStore = HKHealthStore.sharedInstance else { return}
    let config = HKWorkoutConfiguration()
    do {
      workoutSession = try HKWorkoutSession(healthStore: healthStore , configuration: config)
      workoutSession?.delegate = self
      // Start the workout session
      workoutSession?.startActivity(with: Date() )
      startToMeasure()
    } catch { return}
      
  }
  
  func startToMeasure() {
    HKHealthStore.sharedInstance?.execute(self.createStreamingQuery())
  }
  
  private func createStreamingQuery() -> HKQuery {
    let predicate = HKQuery.predicateForSamples(withStart:  Date().addingTimeInterval(-(24*60*60)), end: Date().addingTimeInterval((60*60)), options: [])
    let query = HKAnchoredObjectQuery(type: heartRateType, predicate: predicate, anchor: nil, limit: Int(HKObjectQueryNoLimit)) {
      (query, samples, deletedObjects, anchor, error) -> Void in
      if error == nil {
        self.formatSamples(samples: samples)
      }
    }
    query.updateHandler = { (query, samples, deletedObjects, anchor, error) -> Void in
      if error == nil {
        self.formatSamples(samples: samples)
      }
    }
    return query
  }
  
  private func formatSamples(samples: [HKSample]?) {
    guard let samples = samples as? [HKQuantitySample] else { return }
    guard let quantity = samples.last?.quantity else { return }
    let value = Int(quantity.doubleValue(for: heartRateUnit))
    heartRate = value
    currentHeartRat = "\(heartRate) bpm"
      if session.isReachable {
          let data = ["heartRat": "\(heartRate) bpm"]
          session.sendMessage(data, replyHandler: nil, errorHandler: nil)
      }
  }
}


public extension HKHealthStore {
  class var sharedInstance: HKHealthStore? {
    if !HKHealthStore.isHealthDataAvailable() {
      return nil
    }
    
    struct Singleton {
      static let instance = HKHealthStore()
    }
    
    return Singleton.instance
  }
}


//MARK: WKCrown Delegate
extension WatchConnectViewModel: HKWorkoutSessionDelegate {
  func workoutSession(_ workoutSession: HKWorkoutSession, didChangeTo toState: HKWorkoutSessionState, from fromState: HKWorkoutSessionState, date: Date) {
  }
  
  func workoutSession(_ workoutSession: HKWorkoutSession, didFailWithError error: Error) {
  }
}
