//
//  HealthKitDemoApp.swift
//  HealthKitDemo
//
//  Created by macbook27 on 16/02/24.
//

import SwiftUI

@main
struct HealthKitDemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
