//
//  WatchConnectivityViewModel.swift
//  HealthKitDemo
//
//  Created by macbook27 on 16/02/24.
//

import Foundation
import WatchConnectivity

class WatchConnectivityViewModel: NSObject, ObservableObject, WCSessionDelegate  {
    @Published var reachable: Bool = false
    @Published var recivedStr = ""
    @Published var count = 0
    @Published var heartRatStr = "0"
    
     let session = WCSession.default
     
     override init() {
         super.init()
     }
     
     func establishCOnnection()  {
         if WCSession.isSupported() {
             session.delegate = self
             session.activate()
         }
     }
    
//     func sendDataToWatch() {
//         if session.isReachable {
//             count += 1
//             let data = ["message": "Hello from iOS \(count)"]
//             session.sendMessage(data, replyHandler: nil, errorHandler: nil)
//         }
//     }

     
     func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
         if let error = error {
             print("activation failed with error: \(error.localizedDescription)")
         }
         if WCSession.default.isReachable {
             print("Reachable in app")
         } else {
             print("not reachable in app")
         }
         // Handle activation completion
         print(activationState.rawValue)
     }
     
     func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
         // Handle received message from watch app
         if let message = message["message"] as? String {
             recivedStr = message
             print("Received message from watch app: \(message)")
         }
         if let heartRat = message["heartRat"] as? String {
             DispatchQueue.main.async {
                 self.heartRatStr = heartRat
             }
             print("Received heartrat from watch app: \(heartRat)")

         }
     }
     
     
     func sessionDidBecomeInactive(_ session: WCSession) {
          print("sessionDidBecomeInactive in app")
     }
     
     func sessionDidDeactivate(_ session: WCSession) {
          print("sessionDidDeactivate in app")
     }
     
     func sessionReachabilityDidChange(_ session: WCSession) {
         print("sessionReachabilityDidChange in app")
         print(session.isReachable)
         if WCSession.default.activationState == .activated {
             print("activated in app")
             reachable = true
         }
     }
}
