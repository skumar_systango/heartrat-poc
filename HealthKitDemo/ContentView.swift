//
//  ContentView.swift
//  HealthKitDemo
//
//  Created by macbook27 on 16/02/24.
//

import SwiftUI
import WatchConnectivity

struct ContentView: View {
    @ObservedObject var viewModel = WatchConnectivityViewModel()
    var body: some View {
        VStack(spacing: 20) {
            Image(systemName: "watch")
                .imageScale(.large)
                .foregroundColor(.accentColor)
            Text(viewModel.reachable ? "Rechable" : "Not reachable")
            Text(viewModel.heartRatStr)
                .foregroundColor(.blue)
                .font(.largeTitle)
            
//            Button {
//                viewModel.sendDataToWatch()
//            } label: {
//                Text("Send data to watch")
//            }

        }
        .padding()
        .onAppear{
            viewModel.establishCOnnection()
            print(viewModel.recivedStr)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
